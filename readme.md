# Extensions :
 * Theme : Dark ( Visual studio )
 * Icon pack : VSCode Great Icons
 * Windows opacity
 * Bracket Pair Colorizer 2
 * VSCode Borderless // This is not working after last update
# Settings : 
 * "editor.fontFamily": "'monaco', Consolas",
 * "editor.fontWeight": "500",
 * "editor.fontSize": 15,
 * "window.menuBarVisibility": "toggle",
 * "workbench.iconTheme": "vscode-great-icons",
 * "workbench.colorTheme": "Visual Studio Dark",
 * "editor.minimap.enabled": false,

![Scheme](sc.PNG)
![Scheme](sc1.PNG)
![Scheme](sc2.PNG)